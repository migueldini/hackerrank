package com.hackerrank.tests.FraudulentActivity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class FraudulentActivityNotification {
    // Complete the activityNotifications function below.
    static int activityNotifications(int[] expenditure, int d) {
        int notificationsSent = 0;
        if (expenditure.length <= d) return 0;
        int[] values = Arrays.copyOfRange(expenditure, 0, d);
        for (int i = 0; i < expenditure.length - d; i++) {
            if (i == 0) {
                quicksort(values, 0, d - 1);
            } else {
                substituteAValueInArray(values, expenditure[i - 1], expenditure[i + d - 1]);
            }
            double median = getMedian(values, 0, d);
            if (expenditure[i + d] >= (2 * median)) {
                notificationsSent++;
            }
        }
        return notificationsSent;
    }

    static void quicksort(int[] a, int left, int right) {
        if (left >= right) return;
        int index = partition(a, left, right);
        quicksort(a, left, index - 1);
        quicksort(a, index, right);
    }

    static int partition(int[] a, int left, int right) {
        int pivot = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < pivot) left++;
            while (a[right] > pivot) right--;
            if (left <= right) {
                swap(a, left, right);
                left++;
                right--;
            }
        }
        return left;
    }

    private static void swap(int[] a, int left, int right) {
        int temp = a[left];
        a[left] = a[right];
        a[right] = temp;
    }

    static double getMedian(int[] a, int start, int length) {
        int midValue = (length + start) / 2;
        if (length % 2 != 0) return (double) a[midValue];
        return (a[midValue] + a[midValue - 1]) / 2.0;
    }

    static void substituteAValueInArray(int[] a, int target, int newValue) {
        if (target == newValue) return;
        int index = findTarget(a, target);
        if (a[index] < newValue) {
            while (index < a.length - 1 && a[index] < newValue) {
                a[index] = a[index + 1];
                index++;
            }
        } else {
            while (index > 0 && a[index] > newValue) {
                a[index] = a[index - 1];
                index--;
            }
        }
        a[index] = newValue;
    }

    static boolean isSorted(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) return false;
        }
        return true;
    }

    static int findTarget(int[] a, int target) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int midIndex = low + ((high - low) / 2);
            if (a[midIndex] == target) return midIndex;
            else if (a[midIndex] > target) high = midIndex - 1;
            else low = midIndex + 1;
        }
        return -1;
    }

    static void insertionSort(int[] a, int start, int length) {
        for (int i = start + 1; i < start + length; i++) {
            int j = i;
            int currentValue = a[i];
            while (j > start && currentValue < a[j - 1]) {
                a[j] = a[--j];
            }
            a[j] = currentValue;
        }
    }


    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();
        File testCase = Paths.get(".", "src", "main", "java", "com", "hackerrank", "tests",
                "FraudulentActivity", "TestCase1.txt").toFile();

        Scanner scanner = new Scanner(testCase);

        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);
        int d = Integer.parseInt(nd[1]);

        int[] expenditure = new int[n];

        String[] expenditureItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int expenditureItem = Integer.parseInt(expenditureItems[i]);
            expenditure[i] = expenditureItem;
        }

        int result = activityNotifications(expenditure, d);

        System.out.println(String.valueOf(result));

        System.out.println("Output is " + (result == 633));

        scanner.close();
        System.out.println("Time taken " + (System.currentTimeMillis() - startTime));
    }
}
